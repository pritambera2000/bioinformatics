from flask import Flask, render_template, request
from urllib.parse import quote


app = Flask(__name__)

def is_valid_dna(sequence):
    return all(base in 'ATGC' for base in sequence)

def replicate_dna(dna):
    return dna.replace('A', 't').replace('T', 'a').replace('G', 'c').replace('C', 'g').upper()

def transcribe_dna(dna):
    return dna.replace('T', 'U')

def translate_rna_to_protein(rna):
    rna_codon = {
        "UUU": "F", "CUU": "L", "AUU": "I", "GUU": "V",
        "UUC": "F", "CUC": "L", "AUC": "I", "GUC": "V",
        "UUA": "L", "CUA": "L", "AUA": "I", "GUA": "V",
        "UUG": "L", "CUG": "L", "AUG": "M", "GUG": "V",
        "UCU": "S", "CCU": "P", "ACU": "T", "GCU": "A",
        "UCC": "S", "CCC": "P", "ACC": "T", "GCC": "A",
        "UCA": "S", "CCA": "P", "ACA": "T", "GCA": "A",
        "UCG": "S", "CCG": "P", "ACG": "T", "GCG": "A",
        "UAU": "Y", "CAU": "H", "AAU": "N", "GAU": "D",
        "UAC": "Y", "CAC": "H", "AAC": "N", "GAC": "D",
        "UAA": "STOP", "CAA": "Q", "AAA": "K", "GAA": "E",
        "UAG": "STOP", "CAG": "Q", "AAG": "K", "GAG": "E",
        "UGU": "C", "CGU": "R", "AGU": "S", "GGU": "G",
        "UGC": "C", "CGC": "R", "AGC": "S", "GGC": "G",
        "UGA": "STOP", "CGA": "R", "AGA": "R", "GGA": "G",
        "UGG": "W", "CGG": "R", "AGG": "R", "GGG": "G",
    }

    protein_string = ""
    for i in range(0, len(rna) - (3 + len(rna) % 3), 3):
        if rna_codon[rna[i:i + 3]] == "STOP":
            break
        protein_string += rna_codon[rna[i:i + 3]]

    # Replace 'STOP' with an empty string for HTML rendering
    protein_string = protein_string.replace('STOP', '')

    return protein_string

@app.route('/', methods=['GET', 'POST'])
def index():
    replicated_sequence = ''
    transcribed_rna_sequence = ''
    translated_protein_sequence = ''
    invalid_sequence_message = ''

    if request.method == 'POST':
        dna_sequence = request.form['dna_sequence']
        dna_sequence = dna_sequence.upper()

        if is_valid_dna(dna_sequence):
            replicated_sequence = replicate_dna(dna_sequence)
            transcribed_rna_sequence = transcribe_dna(dna_sequence)
            translated_protein_sequence = translate_rna_to_protein(transcribed_rna_sequence)
        else:
            invalid_sequence_message = 'Enter a valid DNA sequence (containing only A, T, G, and C).'

    return render_template('index.html',
                           replicated_sequence=replicated_sequence,
                           transcribed_rna_sequence=transcribed_rna_sequence,
                           translated_protein_sequence=translated_protein_sequence,
                           invalid_sequence_message=invalid_sequence_message)


